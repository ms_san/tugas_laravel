<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});

Route::get('/data-tables', function () {
    return view('data_table');
});

Route::get('/posts', 'PostController@index');
Route::get('/posts/{post}/show', 'PostController@show');

Route::get('/posts/create', 'PostController@create');
Route::post('/posts/store', 'PostController@store');


Route::get('/posts/{post}/edit', 'PostController@edit');
Route::put('/posts/{post}/update', 'PostController@update');

Route::delete('/posts/{post}/delete', 'PostController@destroy');