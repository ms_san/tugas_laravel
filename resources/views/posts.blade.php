@extends('layouts.app')

@section('content')                   
<div class="container-fluid">
  <div class="d-flex justify-content-between">
    <div>
      <h4>All Post</h4>
      <hr>
    </div>
    <div>
      <a href="/posts/create" class="btn btn-primary">Add new post</a>
    </div>
  </div>
  <div class="row">
    @foreach ($posts as $post)
    <div class="col-md-4">
      <div class="card mb-4">
        <div class="card-header bg-light">
            {{ $post->title }}
        </div>
        <div class="card-body">
          <div>
            {{ Str::limit($post->body, '80', '.') }}
          </div>
          <div>
            <hr>
            <a href="/posts/{{$post->id}}/show">Read more</a>
          </div>
        </div>
      </div>
    </div>
    @endforeach
    <div class="d-felx justify-content-center">
      <div>
        {{ $posts ->links()}}
      </div>
    </div>
    
  </div>
</div>
       
@endsection