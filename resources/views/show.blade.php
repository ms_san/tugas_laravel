@extends('layouts.app')

@section('content')                   
<div class="container">
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h2 class="card-title">{{ $post->title }}</h2>
      </div>
      <div class="card-body">
        {{ $post->body }}
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        <form action="/posts/{{$post->id}}/delete" method="POST">
          @csrf
          @method('delete')
          <a href="/posts/{{$post->id}}/edit" class="btn btn-link text-info p-0 ">Edit</a>
          <button onclick="return confirm('Yakin mau dihapus ?')" type="submit" class="btn btn-link text-danger ml-4">Delete</button>
        </form>
      </div>
      <!-- /.card-footer-->
    </div>
    <!-- /.card -->

  </section>
</div>
       
@endsection